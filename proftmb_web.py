#!/usr/bin/env python3

#from Bio.Blast import NCBIStandalone
import proftmb
import cgitb, cgi
#from Bio.Alphabet import SingleLetterAlphabet
from Bio import SeqIO
import os
import io
import shutil
import tempfile
#cgitb.enable()

CHARLIM = 3000

def print_usage():
	print('Content-type:text/html\n')
	print('''<h2>Usage:</h2>
<table border=1><tr><th>Key</th><th>Type</th><th>Details</th></tr>
<tr><td>q</td><td>String</td><td>Sequence record(s) in FASTA format</td></tr>
<tr><td>evalue</td><td>Float (optional)</td><td>E-value threshold (default: 1e-5)</td></tr>
<tr><td>help</td><td>Boolean</td><td>Print this help page and exit</td></tr>
<tr><td>outfmt</td><td>Enum (optional)</td><td>Output format. Valid options are 'json' (default), 'fasta', or 'hmmtop'</td></tr>
</table>
''')
	exit(0)

def main(seqin, db, expect=1e-5, outfmt='json'):
	tempdir = tempfile.mkdtemp()
	if outfmt == 'hmmtop': print('Content-type:text/plain\n')
	elif outfmt == 'json': print('Content-type:application/json\n')
	elif outfmt == 'fasta': print('Content-type:text/plain\n')
	try: 
		seqlist = list(seqin)
		for i, seq  in enumerate(seqlist):
			proftmb.run_psiblast(tempdir, seqlist[i:i+1], db, expect)
			try: proftmb.run_proftmb(tempdir, seqlist[i:i+1], index=0)
			except FileNotFoundError: pass
			if outfmt == 'hmmtop': 
				print(proftmb.get_hmmtoplike(tempdir, seqlist[i:i+1], index=0).strip())
			elif outfmt == 'json': 
				print(proftmb.get_json(tempdir, seqlist[i:i+1], index=0).strip())
			elif outfmt == 'fasta':
				print(proftmb.get_fasta(tempdir, seqlist[i:i+1], index=0).strip())
	finally: shutil.rmtree(tempdir)
		

if __name__ == '__main__':

	form = cgi.FieldStorage()
	sequence = form.getvalue('q', default='').strip()
	outfmt = form.getvalue('outfmt', default='json')
	if outfmt not in ('json', 'hmmtop', 'fasta'): print_usage()
	if len(sequence) > CHARLIM: sequence = sequence[:CHARLIM]

	if not sequence.strip(): print_usage()

	if not sequence.startswith('>'): sequence = '>untitled\n' + sequence
	sequence += '\n'

	evalue = form.getvalue('evalue', default=1e-5)

	if form.getvalue('help', default=False): print_usage()

	f = tempfile.NamedTemporaryFile()
	f.write(sequence.encode('utf-8'))
	f.flush()
	seqlist = list(SeqIO.parse(f.name, 'fasta'))
	
	#db = os.environ.get('PROFTMB', 'db/knownbeta')
	db = 'db/beta'

	main(seqlist, db, expect=evalue, outfmt=outfmt)
	#webmain(sequence, dbfn, expect=evalue)

