#!/usr/bin/env python3

#from Bio.Blast import NCBIStandalone
from Bio import SeqIO, Seq
import tempfile
import shutil
import argparse
import subprocess
import json
import os

def run_psiblast(outdir, seqlist, dbfa, expect=None):
	for seq in seqlist:	
		newseq = Seq.Seq(str(seq.seq).replace('-', 'X').replace('*', 'X'), seq.seq.alphabet)
		seq.seq = newseq
	with open('{}/seqin.faa'.format(outdir), 'w') as f: SeqIO.write(seqlist, f, 'fasta')

	cmd = ['psiblast']
	if expect is not None: cmd.extend(['-evalue', '{}'.format(expect)])

	cmd.extend(['-query', '{}/seqin.faa'.format(outdir), '-db', dbfa, '-out_ascii_pssm', '{}/out.pssm'.format(outdir), '-save_pssm_after_last_round', '-save_each_pssm', '-parse_deflines'])
	p = subprocess.Popen(cmd, stdout=subprocess.DEVNULL)

	out, err = p.communicate()

def run_proftmb(outdir, seqlist, index=0):
	for i, seq in enumerate(seqlist):
		if i != index: continue

		cmd = ['proftmb', '@/usr/share/proftmb/options', '-q', '{}/out.pssm.{}'.format(outdir, i+1), '-o', '{}/out.proftmb.{}'.format(outdir, i+1)]
		p = subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
		p.communicate()

		os.remove('{}/out.pssm.{}'.format(outdir, i+1))

def get_hmmtoplike(outdir, seqlist, index=0, name='untitled'):
	out = ''
	for i, seq in enumerate(seqlist):
		if i != index: 
			continue

		line = ''
		with open('{}/out.proftmb.{}_proftmb_tabular.txt'.format(outdir, i+1)) as f:
			skip = 1
			for l in f:
				if skip > 0:
					skip -= 1
					continue
				sl = l.split('\t')
				name = seq.name
				length = sl[1]
				start = 'OUT' if sl[7].startswith('O') else ' IN'
				ntmb = sl[6] if sl[6] != '\\0' else '0'

				line += '>BP: {} {} {}  {} '.format(length, name, start, ntmb.ljust(3))
				indices = parse_states(sl[7].strip())
				for tms in indices: 
					line += '{} {} '.format(str(tms[0]).ljust(4), str(tms[1]).ljust(4))
		if not line.strip():
			name = seq.name
			length = len(seq)
			start = ' IN'
			ntmb = '0'
			line += '>BP: {} {} {}  {} '.format(length, name, start, ntmb.ljust(3))
		out += line + '\n'
	return out

def get_json(outdir, seqlist, index=0, name='untitled'):
	objlist = []
	for i, seq in enumerate(seqlist):
		obj = {}
		if i != index: 
			continue

		with open('{}/out.proftmb.{}_proftmb_tabular.txt'.format(outdir, i+1)) as f:
			skip = 1
			for l in f:
				if skip > 0:
					skip -= 1
					continue
				sl = l.split('\t')
				obj['name'] = seq.name
				obj['length'] = int(sl[1])
				obj['start'] = 'OUT' if sl[7].startswith('O') else 'IN'
				obj['count'] = sl[6] if int(sl[6]) != '\\0' else 0
				obj['confidence'] = float(sl[3])/100

				#line += '>BP: {} {} {}  {} '.format(length, name, start, ntmb.ljust(3))
				obj['indices'] = parse_states(sl[7].strip())
				#for tms in indices: 
				#	line += '{} {} '.format(str(tms[0]).ljust(4), str(tms[1]).ljust(4))
		if 'name' not in obj:
			obj['name'] = seq.name
			obj['length'] = len(seq)
			obj['start'] = 'IN'
			obj['count'] = 0
			obj['confidence'] = None
			#line += '>BP: {} {} {}  {} '.format(length, name, start, ntmb.ljust(3))
		objlist.append(obj)
	return json.dumps(objlist)

def get_fasta(outdir, seqlist, index=0, name='untitled'):
	out = ''
	for i, seq in enumerate(seqlist):
		if i != index: 
			continue

		line = ''
		with open('{}/out.proftmb.{}_proftmb_tabular.txt'.format(outdir, i+1)) as f:
			
			skip = 1
			for l in f:
				if skip > 0:
					skip -= 1
					continue
				sl = l.split('\t')
				indices = parse_states(sl[7].strip())
				for j, tms in enumerate(indices):
					out += '>{} TMS{}\n'.format(seq.name, j+1)
					out += '{}\n'.format(seq.seq[tms[0]-1:tms[1]])
	return out



def parse_states(s):
	collapsed = s.replace('I', '-')
	collapsed = collapsed.replace('O', '-')
	collapsed = collapsed.replace('U', 'S')
	collapsed = collapsed.replace('D', 'S')

	last = None
	spans = []
	for i, c in enumerate(collapsed):
		if last is None: last = c
		elif last != c:
			if c == 'S': spans.append([i+1, i+1]) 
		elif last == c and c == 'S':
			spans[-1][-1] += 1
		last = c
	return spans

def main(seqin, seqdb, expect=None, name=None):
	tempdir = tempfile.mkdtemp()
	try: 
		seqlist = list(seqin)
		#run_psiblast(tempdir, seqlist, seqdb, expect)
		#for i, seq in enumerate(seqlist):
		#	run_proftmb(tempdir, seqlist, index=i)
		#	print(get_hmmtoplike(tempdir, seqlist, index=i).strip())
		for i, seq in enumerate(seqlist):
			run_psiblast(tempdir, seqlist[i:i+1], seqdb, expect)
			try: run_proftmb(tempdir, seqlist[i:i+1], index=0)
			except FileNotFoundError: pass
			print(get_hmmtoplike(tempdir, seqlist[i:i+1], index=0).strip())
	finally: shutil.rmtree(tempdir)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-e', '--evalue', default='1e-3', help='E-value threshold (default: 1e-3)')
	#TODO: Implement "fast" mode, i.e. skip PSIBLAST :(
	parser.add_argument('infile')
	args = parser.parse_args()

	db = os.environ.get('PROFTMBDB', './knownbeta')
	seqin = SeqIO.parse(args.infile, 'fasta')
	main(seqin, os.environ['PROFTMBDB'], expect=args.evalue)
