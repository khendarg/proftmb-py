
Running
=======

Inputs
------

 - A FASTA file with any number of records

Outputs
-------

 - An [HMMTOP](http://www.enzim.hu/hmmtop/)-like listing of relevant information, e.g. 

```
>BP: 330 1.B.1.1.9-Q8GD13  IN  16  30   43   53   65   69   80   92   104  107  118  143  155  158  169  181  192  195  206  216  225  228  237  247  259  262  272  282  294  297  308  320  328
```

Note that this program starts its output with ">BP" instead of ">HP".


1. Install the Rost Lab's [PROFtmb](https://rostlab.org/owiki/index.php/PROFtmb_-_Per-residue_and_whole-proteome_prediction_of_bacterial_transmembrane_beta_barrels) somewhere in `$PATH`. If your system is Debian-like, this should be little more than an `apt-get`/`aptitude` away.

2. Make a BLAST database containing beta barrel membrane proteins. We used the [2050 sequences](https://rostlab.org/owiki/index.php/PROFtmb_-_Per-residue_and_whole-proteome_prediction_of_bacterial_transmembrane_beta_barrels#Download_supporting_material) the Rost Lab found to be probable beta barrel membrane proteins.

3. Set `$PROFTMBDB` to the location of said database.

4. Run proftmb.py on 

Known issues
============

This wrapper does not print out PROFtmb's confidence in the results. Interpret results with caution.

References
==========

 - Bigelow, H.R., Petrey, D.S., Liu, J., Przybylski, D., Rost, B. 2004. Predicting transmembrane beta-barrels in proteomes. *Nucleic Acids Res* 32:2566–2577 

